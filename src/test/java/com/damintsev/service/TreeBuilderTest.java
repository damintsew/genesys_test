package com.damintsev.service;

import com.damintsev.domain.Metrics;
import com.damintsev.domain.Node;
import com.damintsev.domain.Tree;
import com.damintsev.service.tree.impl.SimpleTreeBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class TreeBuilderTest {

    private SimpleTreeBuilder builder;

    @Before
    public void init() {
        builder = new SimpleTreeBuilder();
    }

    @Test
    public void BuilderTest() {

        List<String> list = new ArrayList<>(Arrays.asList(
                "/level_1/",
                "/level_1/level_2/",
                "/level_1/level_2_1/",
                "/level_1/level_3_1/",
                "/level_1/level_3_1/level_4_1/"
        ));

        Tree tree = builder.buildTree(new Metrics(list));

        Node root = tree.getRootNode(); // <-- /
        Assert.assertEquals("Root elem should '/'", "/", root.getId());
        Assert.assertEquals("Next elem should 'level_1'", "level_1", root.getChildren().get("level_1").getId());
        Assert.assertEquals("Size of Children 'level_1' should be 3", 3, root.getChildren().get("level_1").getChildren().size());
    }
}
