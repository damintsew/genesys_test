package com.damintsev;


import com.damintsev.service.ZipToJsonService;


/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class Main {

    public static void main(String[] args) {
        new ZipToJsonService().convert(args);
    }
}
