package com.damintsev.config;

import com.damintsev.domain.ParametersHolder;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class FileValidator {

    public void validate(ParametersHolder holder) {

        if (!isFileExists(holder.getInputFilePath())) {
            throw new RuntimeException(String.format("Input file doesn't exists at '%s'. Please provide valid path", holder.getInputFilePath()));
        }
    }

    private boolean isFileExists(String stringPath) {
        Path path = Paths.get(stringPath);
        return Files.exists(path);
    }
}
