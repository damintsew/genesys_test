package com.damintsev.config;

import com.damintsev.domain.ParametersHolder;
import org.apache.commons.cli.*;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class Config {

    private Options options;

    public ParametersHolder parse(String[] arguments) {
        final CommandLineParser parser = new DefaultParser();
        final HelpFormatter formatter = new HelpFormatter();
        final CommandLine cmd;

        try {
            cmd = parser.parse(options, arguments);
        } catch (ParseException e) {
            formatter.printHelp("please provide required parameters", options);

            System.exit(1);
            return null;
        }

        return extractProperties(cmd);
    }

    public Config() {
        init();
    }

    private void init() {
        options = new Options();

        Option input = new Option("i", "input", true, "input file path");
        input.setRequired(true);
        options.addOption(input);

        Option output = new Option("o", "output", true, "output file path");
        output.setRequired(true);
        options.addOption(output);
    }

    private ParametersHolder extractProperties(CommandLine cmd) {

        return new ParametersHolder(cmd.getOptionValue("input"),
                cmd.getOptionValue("output"));
    }
}
