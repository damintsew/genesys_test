package com.damintsev.service;

import com.damintsev.config.Config;
import com.damintsev.config.FileValidator;
import com.damintsev.domain.ParametersHolder;
import com.damintsev.domain.Metrics;
import com.damintsev.domain.Tree;
import com.damintsev.service.filtewriter.FileWriter;
import com.damintsev.service.tree.TreeBuilder;
import com.damintsev.service.zip.ZipDirectoryTraversal;
import com.damintsev.service.zip.impl.ZipDirectoryTraversalImpl;
import com.damintsev.service.filtewriter.impl.JsonFileWriter;
import com.damintsev.service.tree.impl.SimpleTreeBuilder;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class ZipToJsonService {

    public void convert(String []args) {

        Config config = new Config();
        ParametersHolder holder = config.parse(args);

        FileValidator validator = new FileValidator();
        validator.validate(holder);

        ZipDirectoryTraversal traversal = new ZipDirectoryTraversalImpl();
        Metrics metrics = traversal.extract(holder.getInputFilePath());

        TreeBuilder treeBuilder = new SimpleTreeBuilder();
        Tree tree = treeBuilder.buildTree(metrics);

        FileWriter fileWriter = new JsonFileWriter();
        fileWriter.writeToFile(holder.getOutputFilePath(), tree, metrics.getDirectoriesQuantity());
    }
}
