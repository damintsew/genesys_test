package com.damintsev.service.zip;

import com.damintsev.domain.Metrics;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public interface ZipDirectoryTraversal {

    Metrics extract(String holder);
}
