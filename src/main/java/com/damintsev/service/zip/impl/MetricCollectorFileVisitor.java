package com.damintsev.service.zip.impl;

import com.damintsev.domain.Metrics;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class MetricCollectorFileVisitor extends SimpleFileVisitor<Path> {

    private final static String DEFAULT_PATTERN = ".*\\.ctg/$";
    private final String MATCHING_PATTERN;

    private List<String> directories;

    public MetricCollectorFileVisitor() {
        this(DEFAULT_PATTERN);
    }

    public MetricCollectorFileVisitor(String pattern) {
        this.MATCHING_PATTERN = pattern;
        this.directories = new ArrayList<>();
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

        String stringPath = dir.toString();
        if (stringPath.matches(MATCHING_PATTERN)) {
            directories.add(dir.toString());
        }
        return FileVisitResult.CONTINUE;
    }

    public Metrics getMetrics() {
        return new Metrics(directories);
    }
}
