package com.damintsev.service.zip.impl;

import com.damintsev.domain.Metrics;
import com.damintsev.service.zip.ZipDirectoryTraversal;

import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.util.HashMap;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class ZipDirectoryTraversalImpl implements ZipDirectoryTraversal {

    public Metrics extract(String targetFilePath) {

        final MetricCollectorFileVisitor fileVisitor = new MetricCollectorFileVisitor();

        try (FileSystem zipFileSystem = createInMemoryZipFileSystem(targetFilePath)){
            final Path root = zipFileSystem.getPath("/");
            Files.walkFileTree(root, fileVisitor);
        } catch (IOException e) {
            System.err.print("Error during parsing input file:");
            e.printStackTrace();
        }

        return fileVisitor.getMetrics();
    }


    private FileSystem createInMemoryZipFileSystem(String zipFilename) throws IOException {
        // convert the filename to a URI
        final Path path = Paths.get(zipFilename);
        final URI uri = URI.create("jar:file:" + path.toUri().getPath());

        return FileSystems.newFileSystem(uri, new HashMap<>());
    }
}
