package com.damintsev.service.filtewriter;

import com.damintsev.domain.Node;
import com.damintsev.domain.Tree;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public interface FileWriter {

    void writeToFile(String path, Tree tree, Integer quantity);
}
