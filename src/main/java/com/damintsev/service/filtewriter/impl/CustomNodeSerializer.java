package com.damintsev.service.filtewriter.impl;

import com.damintsev.domain.Node;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class CustomNodeSerializer extends JsonSerializer<Node> {
    @Override
    public void serialize(Node node, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();

        jsonGenerator.writeFieldName(node.getId());
        jsonGenerator.writeObject(node.getChildren().values());

        jsonGenerator.writeEndObject();
    }
}
