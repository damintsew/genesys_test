package com.damintsev.service.filtewriter.impl;

import com.damintsev.domain.Node;
import com.damintsev.domain.Tree;
import com.damintsev.service.filtewriter.FileWriter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class JsonFileWriter implements FileWriter {

    public void writeToFile(String path, Tree tree, Integer quantity) {

        ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();

        try {
            writer.writeValue(new File(path), new OutputModel(tree.getRootNode(), quantity));
        } catch (IOException e) {
            System.err.print("Error during storing output file:");
            e.printStackTrace();
        }
    }


    class OutputModel implements Serializable {

        @JsonProperty("directories")
        private Node node;
        private Integer directoriesQuantity;

        public OutputModel(Node node, Integer directoryQuantity) {
            this.node = node;
            this.directoriesQuantity = directoryQuantity;
        }

        public Node getNode() {
            return node;
        }

        public Integer getDirectoriesQuantity() {
            return directoriesQuantity;
        }
    }
}
