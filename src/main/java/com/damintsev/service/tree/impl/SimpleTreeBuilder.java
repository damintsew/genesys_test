package com.damintsev.service.tree.impl;

import com.damintsev.domain.Metrics;
import com.damintsev.domain.Node;
import com.damintsev.domain.Tree;
import com.damintsev.service.tree.TreeBuilder;

import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class SimpleTreeBuilder implements TreeBuilder {

    private static final Pattern PATH_SEPARATOR = Pattern.compile("/");

    public Tree buildTree(Metrics metrics) {
        final List<String> filepaths = metrics.getDirectoriesList();
        final Node root = new Node("/");

        filepaths.forEach(path -> {
            String[] dirs = PATH_SEPARATOR.split(path);

            root.addChild(dirs);
        });

        return new Tree(root);
    }
}
