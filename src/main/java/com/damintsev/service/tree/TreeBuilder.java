package com.damintsev.service.tree;

import com.damintsev.domain.Metrics;
import com.damintsev.domain.Tree;

import java.util.List;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public interface TreeBuilder {

    Tree buildTree(Metrics metrics);
}
