package com.damintsev.domain;

import com.damintsev.service.filtewriter.impl.CustomNodeSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.beans.Transient;
import java.io.Serializable;
import java.util.*;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
@JsonSerialize(using = CustomNodeSerializer.class)
public class Node implements Serializable{

    private String id;
    private Map<String, Node> children;

    public Node(String id) {
        this(id, new HashMap<>());
    }

    public Node(String id, Map<String, Node> children) {
        this.id = id;
        this.children = children;
    }

    public String getId() {
        return id;
    }
    public Map<String, Node> getChildren() {
        return children;
    }

    public void addChild(String[] pathsToAdd) {

        if (pathsToAdd.length == 0) return;
        if (pathsToAdd[0] == null || pathsToAdd[0].equals("")) {
            pathsToAdd = sliceFirst(pathsToAdd);
        }

        if (pathsToAdd.length == 0) return;
        String basePath = pathsToAdd[0];
        pathsToAdd = sliceFirst(pathsToAdd);

        Node childNode  = children.get(basePath);
        if (childNode == null) {
            childNode = new Node(basePath);
            children.put(basePath, childNode);
        }

        if (pathsToAdd.length > 0) {
            childNode.addChild(pathsToAdd);
        }
    }

    //can be improved using start array pointer.
    private String[] sliceFirst(String []arr) {
        return Arrays.copyOfRange(arr, 1, arr.length);
    }

}
