package com.damintsev.domain;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class ParametersHolder {

    private final String inputFilePath;
    private final String outputFilePath;

    public ParametersHolder(String inputFilePath, String outputFilePath) {
        this.inputFilePath = inputFilePath;
        this.outputFilePath = outputFilePath;
    }

    public String getInputFilePath() {
        return inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }
}
