package com.damintsev.domain;

import java.beans.Transient;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class Tree {

    private final Node rootNode;

    public Tree(Node rootNode) {
        this.rootNode = rootNode;
    }

    public Node getRootNode() {
        return rootNode;
    }

}
