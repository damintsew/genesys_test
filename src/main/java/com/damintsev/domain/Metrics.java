package com.damintsev.domain;

import java.util.Collections;
import java.util.List;

/**
 * @author adamintsev
 * @since 18.02.2017.
 */
public class Metrics {

    private final List<String> directoriesList;

    public Metrics(List<String> directoriesList) {
        this.directoriesList = Collections.unmodifiableList(directoriesList);
    }

    public Integer getDirectoriesQuantity() {
        return directoriesList.size();
    }

    public List<String> getDirectoriesList() {
        return directoriesList;
    }

}
